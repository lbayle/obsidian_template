---

excalidraw-plugin: parsed
tags: [excalidraw]

---
==⚠  Switch to EXCALIDRAW VIEW in the MORE OPTIONS menu of this document. ⚠==


# Text Elements
Tests Integration ^ACiJtye8

Tag v1.0 ^LwvAeYeZ

Feature 1.1 ^iunl9fZc

Feature 2.2 ^h9OP9h90

Feature 1.2 ^MvYBvD9G

Release ^bWAdz1uM

Develop ^RUxO03Yq

Hotfix ^cv2ZKVVW

Feature 2.1 ^CgjU8Opl

%%
# Drawing
```json
{
	"type": "excalidraw",
	"version": 2,
	"source": "https://github.com/zsviczian/obsidian-excalidraw-plugin/releases/tag/1.9.2",
	"elements": [
		{
			"type": "arrow",
			"version": 145,
			"versionNonce": 1915046050,
			"isDeleted": false,
			"id": "r-pOVNcbvW0vwVTxicrH9",
			"fillStyle": "hachure",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": -291,
			"y": -132.2265625,
			"strokeColor": "#1971c2",
			"backgroundColor": "transparent",
			"width": 878,
			"height": 8,
			"seed": 390974562,
			"groupIds": [],
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1685630640742,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					878,
					-8
				]
			]
		},
		{
			"type": "arrow",
			"version": 141,
			"versionNonce": 606767742,
			"isDeleted": false,
			"id": "_rShY7FvBp3U7cnE4PzCd",
			"fillStyle": "hachure",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": -396.5,
			"y": 10.7734375,
			"strokeColor": "#e03131",
			"backgroundColor": "transparent",
			"width": 972,
			"height": 3,
			"seed": 796625762,
			"groupIds": [],
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1685630640743,
			"link": null,
			"locked": false,
			"startBinding": {
				"elementId": "bWAdz1uM",
				"focus": 0.6173396198166952,
				"gap": 7.09500639173001
			},
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					972,
					-3
				]
			]
		},
		{
			"type": "arrow",
			"version": 150,
			"versionNonce": 1903682658,
			"isDeleted": false,
			"id": "U7kAtYl3yCuUGgn-YoosZ",
			"fillStyle": "hachure",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": -342.5,
			"y": 8.7734375,
			"strokeColor": "#1971c2",
			"backgroundColor": "transparent",
			"width": 51,
			"height": 141,
			"seed": 202913662,
			"groupIds": [],
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1685630640743,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					51,
					-141
				]
			]
		},
		{
			"type": "line",
			"version": 236,
			"versionNonce": 1840286398,
			"isDeleted": false,
			"id": "bI8F0W_Z55OH-THNSyRN9",
			"fillStyle": "hachure",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": -240,
			"y": -133.2265625,
			"strokeColor": "#2f9e44",
			"backgroundColor": "transparent",
			"width": 169,
			"height": 97,
			"seed": 1614144446,
			"groupIds": [],
			"roundness": null,
			"boundElements": [],
			"updated": 1685630640743,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					25,
					-97
				],
				[
					139,
					-97
				],
				[
					169,
					-1
				]
			]
		},
		{
			"type": "line",
			"version": 312,
			"versionNonce": 1803618338,
			"isDeleted": false,
			"id": "pUXW_H2_pfwdURrdjobOM",
			"fillStyle": "hachure",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": -51.5,
			"y": -134.7265625,
			"strokeColor": "#2f9e44",
			"backgroundColor": "transparent",
			"width": 169,
			"height": 97,
			"seed": 1443520802,
			"groupIds": [],
			"roundness": null,
			"boundElements": [],
			"updated": 1685630640743,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					25,
					-97
				],
				[
					139,
					-97
				],
				[
					169,
					-1
				]
			]
		},
		{
			"type": "arrow",
			"version": 147,
			"versionNonce": 1326220030,
			"isDeleted": false,
			"id": "m174IA-AQ1RprcdoCdfjh",
			"fillStyle": "hachure",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": 135,
			"y": -134.2265625,
			"strokeColor": "#1971c2",
			"backgroundColor": "transparent",
			"width": 66.65091587439358,
			"height": 140.62921234143437,
			"seed": 2010613858,
			"groupIds": [],
			"roundness": null,
			"boundElements": [],
			"updated": 1685630640743,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": "triangle",
			"points": [
				[
					0,
					0
				],
				[
					66.65091587439358,
					140.62921234143437
				]
			]
		},
		{
			"type": "text",
			"version": 241,
			"versionNonce": 2144495870,
			"isDeleted": false,
			"id": "ACiJtye8",
			"fillStyle": "hachure",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": 241.77731086142194,
			"y": 22.306630758426536,
			"strokeColor": "#e03131",
			"backgroundColor": "transparent",
			"width": 180.57980346679688,
			"height": 25,
			"seed": 1233318398,
			"groupIds": [],
			"roundness": null,
			"boundElements": [],
			"updated": 1685631276008,
			"link": null,
			"locked": false,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "Tests Integration",
			"rawText": "Tests Integration",
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "Tests Integration",
			"lineHeight": 1.25,
			"baseline": 18
		},
		{
			"type": "arrow",
			"version": 571,
			"versionNonce": 1254060862,
			"isDeleted": false,
			"id": "bKQuTPA_NkQdVzGnmXlz9",
			"fillStyle": "hachure",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": 541.3209247206332,
			"y": 2.850790009283969,
			"strokeColor": "#1971c2",
			"backgroundColor": "transparent",
			"width": 27.795881271860537,
			"height": 140.75508359542619,
			"seed": 398059326,
			"groupIds": [],
			"roundness": null,
			"boundElements": [],
			"updated": 1685630640743,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": "triangle",
			"points": [
				[
					0,
					0
				],
				[
					27.795881271860537,
					-140.75508359542619
				]
			]
		},
		{
			"type": "rectangle",
			"version": 112,
			"versionNonce": 1502799778,
			"isDeleted": false,
			"id": "darw3IUT8BSQuOZS354jT",
			"fillStyle": "hachure",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": 479.038654681631,
			"y": 69.7734375,
			"strokeColor": "#e03131",
			"backgroundColor": "transparent",
			"width": 78,
			"height": 56,
			"seed": 795143394,
			"groupIds": [],
			"roundness": null,
			"boundElements": [
				{
					"type": "text",
					"id": "LwvAeYeZ"
				}
			],
			"updated": 1685630640743,
			"link": null,
			"locked": false
		},
		{
			"type": "text",
			"version": 103,
			"versionNonce": 1797938046,
			"isDeleted": false,
			"id": "LwvAeYeZ",
			"fillStyle": "hachure",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": 499.137287494131,
			"y": 74.7734375,
			"strokeColor": "#e03131",
			"backgroundColor": "transparent",
			"width": 37.802734375,
			"height": 46,
			"seed": 336108130,
			"groupIds": [],
			"roundness": null,
			"boundElements": [],
			"updated": 1685630640743,
			"link": null,
			"locked": false,
			"fontSize": 20,
			"fontFamily": 2,
			"text": "Tag \nv1.0",
			"rawText": "Tag v1.0",
			"textAlign": "center",
			"verticalAlign": "middle",
			"containerId": "darw3IUT8BSQuOZS354jT",
			"originalText": "Tag v1.0",
			"lineHeight": 1.15,
			"baseline": 41
		},
		{
			"type": "line",
			"version": 184,
			"versionNonce": 2010751842,
			"isDeleted": false,
			"id": "0mz0tkjTlX1Zh9q2YIHJJ",
			"fillStyle": "hachure",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": 513.0386546816312,
			"y": 66.59570641385778,
			"strokeColor": "#e03131",
			"backgroundColor": "transparent",
			"width": 0,
			"height": 55,
			"seed": 2099117182,
			"groupIds": [],
			"roundness": null,
			"boundElements": [],
			"updated": 1685630640743,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					0,
					-55
				]
			]
		},
		{
			"type": "text",
			"version": 28,
			"versionNonce": 1183726526,
			"isDeleted": false,
			"id": "iunl9fZc",
			"fillStyle": "hachure",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": -210.4218486891375,
			"y": -259.93748684456875,
			"strokeColor": "#2f9e44",
			"backgroundColor": "transparent",
			"width": 102.28515625,
			"height": 23,
			"seed": 2000500414,
			"groupIds": [],
			"roundness": null,
			"boundElements": [],
			"updated": 1685630640743,
			"link": null,
			"locked": false,
			"fontSize": 20,
			"fontFamily": 2,
			"text": "Feature 1.1",
			"rawText": "Feature 1.1",
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "Feature 1.1",
			"lineHeight": 1.15,
			"baseline": 18
		},
		{
			"type": "text",
			"version": 86,
			"versionNonce": 147136638,
			"isDeleted": false,
			"id": "h9OP9h90",
			"fillStyle": "hachure",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": 217.466117527174,
			"y": -262.3787364130435,
			"strokeColor": "#2f9e44",
			"backgroundColor": "transparent",
			"width": 102.28515625,
			"height": 23,
			"seed": 55400994,
			"groupIds": [],
			"roundness": null,
			"boundElements": [],
			"updated": 1685637807391,
			"link": null,
			"locked": false,
			"fontSize": 20,
			"fontFamily": 2,
			"text": "Feature 2.2",
			"rawText": "Feature 2.2",
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "Feature 2.2",
			"lineHeight": 1.15,
			"baseline": 18
		},
		{
			"type": "text",
			"version": 64,
			"versionNonce": 51123198,
			"isDeleted": false,
			"id": "MvYBvD9G",
			"fillStyle": "hachure",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": -24.142578125,
			"y": -262.7265625,
			"strokeColor": "#2f9e44",
			"backgroundColor": "transparent",
			"width": 102.28515625,
			"height": 23,
			"seed": 1907327294,
			"groupIds": [],
			"roundness": null,
			"boundElements": [],
			"updated": 1685630640743,
			"link": null,
			"locked": false,
			"fontSize": 20,
			"fontFamily": 2,
			"text": "Feature 1.2",
			"rawText": "Feature 1.2",
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "Feature 1.2",
			"lineHeight": 1.15,
			"baseline": 18
		},
		{
			"type": "line",
			"version": 324,
			"versionNonce": 309179106,
			"isDeleted": false,
			"id": "zpaB9KB1LRh7_WbpiCVli",
			"fillStyle": "hachure",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": 187.5,
			"y": -139.7265625,
			"strokeColor": "#2f9e44",
			"backgroundColor": "transparent",
			"width": 169,
			"height": 97,
			"seed": 1884361378,
			"groupIds": [],
			"roundness": null,
			"boundElements": [],
			"updated": 1685630640743,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					25,
					-97
				],
				[
					139,
					-97
				],
				[
					169,
					-1
				]
			]
		},
		{
			"type": "text",
			"version": 49,
			"versionNonce": 1520755774,
			"isDeleted": false,
			"id": "bWAdz1uM",
			"fillStyle": "hachure",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": -506.32547514173,
			"y": -15.183167272549184,
			"strokeColor": "#e03131",
			"backgroundColor": "transparent",
			"width": 102.73046875,
			"height": 32.199999999999996,
			"seed": 609389310,
			"groupIds": [],
			"roundness": null,
			"boundElements": [
				{
					"id": "_rShY7FvBp3U7cnE4PzCd",
					"type": "arrow"
				}
			],
			"updated": 1685630640743,
			"link": null,
			"locked": false,
			"fontSize": 28,
			"fontFamily": 2,
			"text": "Release",
			"rawText": "Release",
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "Release",
			"lineHeight": 1.15,
			"baseline": 25
		},
		{
			"type": "text",
			"version": 161,
			"versionNonce": 968427170,
			"isDeleted": false,
			"id": "RUxO03Yq",
			"fillStyle": "hachure",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": -422.25163461037835,
			"y": -157.4113178343321,
			"strokeColor": "#1971c2",
			"backgroundColor": "transparent",
			"width": 102.73046875,
			"height": 32.199999999999996,
			"seed": 771410302,
			"groupIds": [],
			"roundness": null,
			"boundElements": [],
			"updated": 1685630640743,
			"link": null,
			"locked": false,
			"fontSize": 28,
			"fontFamily": 2,
			"text": "Develop",
			"rawText": "Develop",
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "Develop",
			"lineHeight": 1.15,
			"baseline": 25
		},
		{
			"type": "line",
			"version": 588,
			"versionNonce": 523873406,
			"isDeleted": false,
			"id": "qeb5-RD3L1IBcA1LnCvZk",
			"fillStyle": "hachure",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": 326.627040963034,
			"y": 7.295824487753926,
			"strokeColor": "#e03131",
			"backgroundColor": "transparent",
			"width": 104.22479026217957,
			"height": 44.002101123601506,
			"seed": 1549893246,
			"groupIds": [],
			"roundness": null,
			"boundElements": [],
			"updated": 1685630640743,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					15.417868381979225,
					-44.00210112360151
				],
				[
					85.72334820380449,
					-44.00210112360151
				],
				[
					104.22479026217957,
					-0.45362990849073404
				]
			]
		},
		{
			"type": "text",
			"version": 27,
			"versionNonce": 1273225826,
			"isDeleted": false,
			"id": "cv2ZKVVW",
			"fillStyle": "hachure",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": 351.06275556978267,
			"y": -64.86972157965866,
			"strokeColor": "#e03131",
			"backgroundColor": "transparent",
			"width": 51.123046875,
			"height": 23,
			"seed": 860650494,
			"groupIds": [],
			"roundness": null,
			"boundElements": [],
			"updated": 1685630640743,
			"link": null,
			"locked": false,
			"fontSize": 20,
			"fontFamily": 2,
			"text": "Hotfix",
			"rawText": "Hotfix",
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "Hotfix",
			"lineHeight": 1.15,
			"baseline": 18
		},
		{
			"id": "pGlD4iw5MjShxDq1GRdYX",
			"type": "line",
			"x": -62.2714332230388,
			"y": -134.15183423913044,
			"width": 423.4782608695652,
			"height": 162.60869565217388,
			"angle": 0,
			"strokeColor": "#2f9e44",
			"backgroundColor": "transparent",
			"fillStyle": "hachure",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"groupIds": [],
			"roundness": null,
			"seed": 1581327358,
			"version": 164,
			"versionNonce": 1756643646,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1685637791873,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					21.472137170851145,
					-162.60869565217388
				],
				[
					423.4782608695652,
					-162.60869565217388
				]
			],
			"lastCommittedPoint": [
				308.695652173913,
				-162.60869565217388
			],
			"startBinding": null,
			"endBinding": null,
			"startArrowhead": null,
			"endArrowhead": null
		},
		{
			"type": "text",
			"version": 187,
			"versionNonce": 1479896930,
			"isDeleted": false,
			"id": "CgjU8Opl",
			"fillStyle": "hachure",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": 250.93381473891748,
			"y": -320.43444293478245,
			"strokeColor": "#2f9e44",
			"backgroundColor": "transparent",
			"width": 102.28515625,
			"height": 23,
			"seed": 592405538,
			"groupIds": [],
			"roundness": null,
			"boundElements": [],
			"updated": 1685637805149,
			"link": null,
			"locked": false,
			"fontSize": 20,
			"fontFamily": 2,
			"text": "Feature 2.1",
			"rawText": "Feature 2.1",
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "Feature 2.1",
			"lineHeight": 1.15,
			"baseline": 18
		}
	],
	"appState": {
		"theme": "dark",
		"viewBackgroundColor": "#ffffff",
		"currentItemStrokeColor": "#2f9e44",
		"currentItemBackgroundColor": "transparent",
		"currentItemFillStyle": "hachure",
		"currentItemStrokeWidth": 2,
		"currentItemStrokeStyle": "solid",
		"currentItemRoughness": 0,
		"currentItemOpacity": 100,
		"currentItemFontFamily": 2,
		"currentItemFontSize": 20,
		"currentItemTextAlign": "left",
		"currentItemStartArrowhead": null,
		"currentItemEndArrowhead": "triangle",
		"scrollX": 539.6627375708649,
		"scrollY": 473.75781249999994,
		"zoom": {
			"value": 1.1500000000000001
		},
		"currentItemRoundness": "sharp",
		"gridSize": null,
		"currentStrokeOptions": null,
		"previousGridSize": null
	},
	"files": {}
}
```
%%