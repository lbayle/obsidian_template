***Sommaire***
```toc
style: number 
min_depth: 1 
max_depth: 2
```
---
## Formatage du texte

Du texte normal,  *italique* ,  **gras** ,  ==stabiloté== ,  ~~barré~~
un `mot clef`
un tag #git  facilite les recherches
<font color=green>Du texte en couleur avec du CSS</font>
- [ ] case à cocher

> du texte mis en avant

Exporter en PDF: `Ctrl+P PDF`

---
## Les liens
vers une autre page: [[Git - commandes utiles]]
vers un paragraphe: [[#Formatage du texte]] 
vers une URL : [GitLab SEAMIS](https://gitlab-atos64/dashboard/projects)
vers un fichier local: [hosts (windows)](file:///C:/Windows/System32/drivers/etc/hosts) ou [hosts (linux)](file:///etc/hosts)

Un lien est défini entre `[[`doubles crochets `]]` et concerne pages, images, documents, ...
Un point d'exclamation  devant le lien `![[` permet d'inclure son contenu dans la page courante

---
## Tableaux
pour faire un tableau:
- click souris droit dans la page

![[Pasted image 20240217133140.png]]

---
## Blocs de code
```bash
git clone ssh://gitlab.sics:5001/SICS/engineering/obsidian.git
```

```java
class HelloWorld {
    public static void main(String[] args) {
        System.out.println("Hello, World!"); 
    }
}
```
---
## Blocs de texte

> [!attention]
> une note commence par `>[!note] `

##### Liste des possibilités
- note, info, todo
- abstract, summary, tldr
- tip, hint, important
- success, check, done
- question, help, faq
- warning, caution, attention
- failure, fail, missing, danger, error, bug
- example, quote, cite

---
## Modèles de documents
- composer un modèle dans le dossier `zzz_data/modeles`
- dans le menu de gauche choisir `insérer un modèle`

Celui-ci peut contenir des meta-data qui seront traités par le plugin [[#Dataview]]

---
## Dataview
Permet de construire des pages dynamiquement avec des requetes SQL ou du Javascript !
[Aide en ligne](https://blacksmithgu.github.io/obsidian-dataview/)

```sql
TABLE Projet, Type, Auteur, Date_debut FROM "REX/Livraisons" WHERE file.name != "_Sommaire" SORT Date_debut DESC LIMIT 3
```

```dataview
TABLE file.mtime AS "modifié", Auteur, file.folder AS "Path"  SORT file.mtime DESC LIMIT 3
```
---
## Notes Quotidiennes

Pour créer des Notes quotidiennes "Perso"
- Créer un dossier `_PERSO/DailyNotes`  ( `_PERSO` pour ne pas qu'il soit poussé dans git )
- Configurer le plugin **"Notes Quotidiennes"** avec:
    - Nouvel emplacement de fichier = `_PERSO/DailyNotes`
    - Emplacement du fichier de modèle = `_PERSO/DailyNotes/modele_NoteQuotidienne`   (Optionnel)

---
## Images
- copier/coller depuis le presse-papier
- ou déposer l'image dans `zzz_data/images`
- changer la taille (nb pixels): `![[image.png|250]]`
- Après un copier/coller, on peut renommer le fichier .PNG à partir du menu contextuel (sur le lien lui-même)

![[Install_obsidian_2.png|400]]


---
## Excalidraw
`Ctrl+P`  "Excalidraw" "IN A NEW PANE"

![[exemple.excalidraw]]


---
## Diagrams and flowcharts

### Mermaid

Syntaxe [Mermaid](https://mermaid.js.org/syntax/classDiagram.html)

```mermaid
graph TD
    Trompette --- Gaelle
    Trompette --> Mariane
```

```mermaid
graph LR

    subgraph "subgraph"
    C(Round Rect)
    B((Circle))
    end
    
    A[Square Rect] -- Link text --> B
    A -.-> C
    B --> D{Losange}
    C --> D
    D ==> E([Bords très arondis])
```

---

##### class diagram 
```mermaid
classDiagram
    Animal <|-- Duck
    Animal <|-- Fish
    Animal <|-- Zebra
    Animal : +int age
    Animal : +String gender
    Animal: +isMammal()
    Animal: +mate()
    class Duck{
      +String beakColor
      +swim()
      +quack()
    }
    class Fish{
      -int sizeInFeet
      -canEat()
    }
    class Zebra{
      +bool is_wild
      +run()
    }
```

---
##### state diagram
```mermaid
stateDiagram-v2
    [*] --> Still
    Still --> [*]
    Still --> Moving
    Moving --> Still
    Moving --> Crash
    Crash --> [*]
```
---
#### git branches

```mermaid
gitGraph
    commit id: "engie - msg 1"
    commit id: "issue #12345"
    branch develop
    checkout develop
    commit id: "1"
    commit
    checkout main
    commit id: "2"
    merge develop id: "merge"
    commit id: "3" tag: "1.2-RC.9"
    commit id: "reverse commit" type: REVERSE
    
```

---
##### pie diagram
```mermaid
pie title Pets adopted by volunteers
    "Dogs" : 386
    "Cats" : 85
    "Rats" : 15
```

---

# imbedd PDF & Youtube

- pour qu'un lien vers un PDF puisse être affiché dans une page, il doit être inclu dans le Vault (oui, c'est dommage)
- pour youtube: copier le lien

![Janco - Sciences-Po](https://www.youtube.com/watch?v=h9SuWi_mtCM)


