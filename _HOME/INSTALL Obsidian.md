#### Télécharger et installer Obsidian

Site officiel: [https://obsodian.md](https://obsidian.md/)

#### Checkout du repo git

```bash
cd ~/Documents
git clone git@gitlab.com:your_name/obsidian.git
```

#### Lancer Obsidian
```bash
chmod a+x Obsidian-1.4.13.appimage

./Obsidian-1.4.13.appimage  # si erreur, ajouter l'option --no-sandbox
```

#### Ouvrir le vault

1. lancer obsidian
2. `Ouvrir` un dossier comme coffre ( en anglais: `Open folder as vault` )
3. sélectionner le répertoire `~/Documents/obsidian`                                  
4. activer les modules: `Faire confiance à l'auteur et activer les modules`


![[Install_obsidian_1.png|380]]

![[Install_obsidian_2.png|430]]

# post-install

>[!Info]
>Obsidian sauvegarde sa configuration dans le repertoire `.obsidian`
>Une partie des paramètres sont pré-configurés et sont communs à l'équipe.
> => Demander à l'équipe ce qui peut être modifié *(voir aussi le .gitignore)*

## Thème
Il est conseillé d'utiliser le thème `Comfort color dark`, mais vous pouvez en télécharger d'autres :

![](Install_obsidian_3.png)

## Espace _PERSO
le répertoire `_PERSO` est exclu de la sauvegarde sur Git.
vous pouvez y déposer vos notes/fichiers perso.

extrait du `.gitignore`
```ini
# any file or folder starting with '_perso' will be ignored
**/_perso*
**/_Perso*
**/_PERSO*
```

***Notes Quotidiennes***
- Elles sont stockées dans le dossier `_PERSO/DailyNotes`  ( Le plugin est pré-configuré sur cet emplacement )
- Click sur l'icone `Ouvrir la note quotidienne` dans le *menu de gauche*

## HOWTO
Pour apprendre à vous servir d'Obsidian, vous pouvez commencer par aller voir la page [[HOWTO Obsidian]], qui montre un peu les possibilités de l'outil.
